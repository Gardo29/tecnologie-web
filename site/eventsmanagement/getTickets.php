<?php 
require_once("../bootstrap.php");
if(!isset($_POST["event_id"],$_SESSION["user_id"])){
    echo "-1";
    exit;
}

$dbResult = $db->getTicket($_POST["event_id"],$_SESSION["user_id"]);
if($dbResult){
    echo $dbResult[0]["number_of_tickets"];
    exit;
}
echo "-1";
?>
