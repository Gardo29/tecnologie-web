let offset = 0;
let limit = 5;

$(document).ready(function(){
    drawAndIncrease();
    $(window).bind('scroll', function() {
        if($(window).scrollTop() > $("main.eventmanager").offset().top + $("main.eventmanager").outerHeight() - window.innerHeight) {
            drawAndIncrease(function(){});
        }
    });
    $("select#owner").change(function(){
        offset = 0;
        func = function(){};
        
        $("main.eventmanager section.events").empty();
        drawAndIncrease(func);
    });
});

function drawAndIncrease(){
    func = function(){};
    if($("select#owner").val() === "Attended"){
        func = function(event){
            removeButtons(event);
            addInfo(event);
        }
    }
    drawEvent(offset,limit,func);
    offset+=limit;
}
function removeButtons(event){
    event.find("button").remove();
    
}
function addInfo(singleEvent){
    $.post("eventsmanagement/getTickets.php",{"event_id":singleEvent.find("p.event-id").html()}).
    done(function(result){
        if(result>0){
            const price = singleEvent.find("span.event-price");
            const totalPrice = parseInt(result)*parseInt(price.html());
            console.log(totalPrice);
            
            price.parent().html(price.parent().html()+"<br> Total: "+totalPrice+" €");
            singleEvent.find("div.buttons").append($(`<p>Tickets: ${result}</p>`));
        }
    });
}

