<?php 
define("SUCCESS","0");
define("USERNAME_ALREADY_TAKEN","1");
define("EMPTY_FIELD","2");
define("WRONG_PASSWORD","3");
define("INEXISTING_USER","4");
define("DISABLED_USER","5");
define("DB_ERROR","6");

define("MAX_ATTEMPT","5");

function sec_session_start() {
   $session_name = "session_E20"; // Imposta un nome di sessione TODO: da cambiare
   $secure = false; // Imposta il parametro a true se vuoi usare il protocollo "https".
   $httponly = true; // Questo impedirà ad un javascript di essere in grado di accedere all"id di sessione.
   ini_set("session.use_only_cookies", 1); // Forza la sessione ad utilizzare solo i cookie.
   $cookieParams = session_get_cookie_params(); // Legge i parametri correnti relativi ai cookie.
   setcookie("ciao","ciao",time()*60);
   session_set_cookie_params($cookieParams["lifetime"], $cookieParams["path"], $cookieParams["domain"], $secure, $httponly); 
   session_name($session_name); // Imposta il nome di sessione con quello prescelto all"inizio della funzione.
   session_start(); // Avvia la sessione php.
   session_regenerate_id(); // Rigenera la sessione e cancella quella creata in precedenza.
}

function login($email, $password) {
   global $db;
   $user = $db->searchUser($email);
   if($user === false){
      return DB_ERROR;
   }
   if(count($user) == 0){
      return INEXISTING_USER;
   }
   $user = $user[0];
   // verifichiamo che non sia disabilitato in seguito all"esecuzione di troppi tentativi di accesso errati.
   if(!$user["is_active"] || checkbrute($email) == true) { 
      return DISABLED_USER;
   }
   $password = hash("sha512", $password.$user["salt"]); // codifica la password usando una chiave univoca.
   if($user["password"] != $password) {
      $db->createNewLoginAttempt($user["email"]);
      return WRONG_PASSWORD;
   }
   // Password corretta!            
   $_SESSION["user_id"] = $user["email"];
   $_SESSION["login_string"] = hash("sha512", $password.$_SERVER["HTTP_USER_AGENT"]);
   $db->removeAttempts($user["email"]);
   return SUCCESS; 
}
function checkbrute($user_id) {
   // Vengono analizzati tutti i tentativi di login a partire dalle ultime due ore.
   global $db;
   $hoursLeft = 3;
   $pastTime = (new DateTime())->sub(new DateInterval("PT{$hoursLeft}H"));
   $dbResult = $db->getLoginByTime($user_id,$pastTime);
   if(count($dbResult) >= MAX_ATTEMPT){
      $db->setUserState($user_id,false);
      return true;
   }
   return false;
}
function loginCheck() {
   global $db;
   // Verifica che tutte le variabili di sessione siano impostate correttamente
   if(!isset($_SESSION["user_id"], $_SESSION["login_string"])) {
      return false;
   }
   $user = $db->searchUser($_SESSION["user_id"]);
   if (!$user || count($user) != 1) {
      return false;
   }
   $user = $user[0];
   $login_check = hash("sha512", $user["password"].$_SERVER["HTTP_USER_AGENT"]);
   if($login_check != $_SESSION["login_string"]) {
      return false;
   }
   return true;  
}

function registration($email,$password,$name,$surname){
   global $db;
   $user = $db->searchUser($email);

   if($user === false){
      return DB_ERROR;
   }
   if(count($user) != 0){
      return USERNAME_ALREADY_TAKEN;
   }
   $password = createPassword($password);
   // Inserisci a questo punto il codice SQL per eseguire la INSERT nel tuo database
   // Assicurati di usare statement SQL "prepared".
   if($db->registerUser($email,$password["password_cript"],$name,$surname,$password["salt"])){
      return SUCCESS;
   }
   return DB_ERROR;
}
function createPassword($password){
   // Crea una chiave casuale
   $random_salt = hash("sha512", uniqid(mt_rand(1, mt_getrandmax()), true));
   // Crea una password usando la chiave appena creata.
   $password = hash("sha512", $password.$random_salt);
   return array("password_cript"=>$password,"salt"=>$random_salt);
}
function changeCredentials($email,$password,$name,$surname){
   global $db;
   $user = $db->searchUser($email);

   if($user === false){
      return DB_ERROR;
   }
   if(count($user) != 0 && $user[0]["email"] != $email){
      return USERNAME_ALREADY_TAKEN;
   }
   $password = createPassword($password);
   if($db->changeUser($_SESSION["user_id"],$email,$password["password_cript"],$password["salt"],$name,$surname)){
      rename("../".USER_RES.$_SESSION["user_id"],"../".USER_RES.$email);
      return SUCCESS;
   }
   return DB_ERROR;
}

