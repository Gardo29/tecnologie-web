<?php
require_once("../bootstrap.php");

if(!isset($_POST["request"],$_POST["email"]) || ($_POST["request"] !== "login" && $_POST["request"] !== "registration" && $_POST["request"] !== "profile")){
    echo false;
    exit;
}
// LOGIN
if($_POST["request"] === "login"){
    $user = $db->searchUser($_POST["email"]);
    if($user === false){
       echo "DB_ERROR";
       exit;
    }
    if(count($user) == 0){
       echo "INEXISTING_USER";
       exit;
    }
    echo "SUCCESS";
    exit;
}
// REGISTRATION
if($_POST["request"] === "registration"){
    $user = $db->searchUser($_POST["email"]);
    if($user === false){
       echo "DB_ERROR";
       exit;
    }
    if(count($user) !== 0){
       echo "USERNAME_ALREADY_TAKEN";
       exit;
    }
    echo "SUCCESS";
    exit;
}

if($_POST["request"] === "profile"){

    $user = $db->searchUser($_POST["email"]);
    if($user === false){
       echo "DB_ERROR";
       exit;
    }

    if(count($user) !== 0 && $user[0]["email"] !== $_POST["email"]){
       echo "USERNAME_ALREADY_TAKEN";
       exit;
    }
    echo "SUCCESS";
    exit;
}
?>
