$(document).ready(function(){
    $("#continue").click(function(e){
        e.preventDefault();
        const userEmail = $("form input[type=\"email\"]").val()
        if(!checkInput($("form input")) || !isMail(userEmail) || !validPassword($("form input[type=\"password\"]").val())){
            return;
        }
        if($("main.profile form input#registration-email").length){
            checkUsername(userEmail,"profile");
        }
        else if($("form input#registration-email").length){
           checkUsername(userEmail,"registration");
        }
        if($("form input#login-email").length){
            checkUsername(userEmail,"login");
        } 
    });
});
function formhash(form, password) {
    if(!checkInput(form)){ 
        return;
    }
    var encryptedPassword = document.createElement("input");
    form.append(encryptedPassword);
    encryptedPassword.name = "encryptedPassword";
    encryptedPassword.type = "hidden"
    encryptedPassword.value = hex_sha512(password.val());
    password.val("");
    form.submit();
 }
 function checkUsername(email,request){
    $.post("login/registrationAPI.php",{"request":request,"email":email}).done(function(data){
        console.log(data);
        manageResult(data);
    });
 }
 function manageResult(error){
    let errorString = null;
    switch(error){
        case "USERNAME_ALREADY_TAKEN":
            errorString = "Username Already Taken";
        break;
        case "DB_ERROR":
            errorString = "Database Error";
        break;
        case "INEXISTING_USER":
            errorString = "Inexisting User";
        break;
        case "SUCCESS":
            formhash($("form"),$("form input[type=\"password\"]")); 
        return;
    }
    if($("section.error-box p").length){
        $("section.error-box p").html(errorString);
    }
    else{
        $("form").prev().after($("<section class=\"error-box\"><p>"+errorString+"</p></section>"));
    }

}




