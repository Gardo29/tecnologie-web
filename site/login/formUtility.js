$(document).ready(function(){
    $("form input").on("input selectionchange propertychange",function(){
        $(this).removeClass("input-error");
    });

    $("form input[type=\"mail\"]").focusout(function(){
        if(!isMail($(this).val())){
            $(this).addClass("input-error");
        }
    });  
    $("form input[type=\"password\"]").focusout(function(){
        if(!validPassword($(this).val())){
            $(this).addClass("input-error");
        }
    });
    $("form input").focusout(function(){
        const value = $(this).val();
        if(value === "" || value === undefined){
            $(this).addClass("input-error");
        }
    });
    $("#continue").click(function(e){
        e.preventDefault();
        $("form input").each(function(){
            const value = $(this).val();
            if(value === "" || value === undefined){
                $(this).addClass("input-error");
            }
        });
    });

    
});
function isMail(mailValue){
    const regExpr = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/; 
    if(mailValue === '' || mailValue === undefined || !regExpr.test(mailValue)){    
        return false;
    }
    return true;
}
function validPassword(password){
    const passwordSize = 8;
    const regExp = /^\w+$/;
    if(password === "" || password === undefined || !regExp.test(password) || password.length < passwordSize){
        return false;
    }
    return true;
}

function checkInput(form){
    let cond = true;
    form.children("input").each(function(){
        const value = this.value;
        if(value === "" || value === undefined){
            cond = false;
        }
    });
    return cond;
}

function folderNameChanger(folder,newName){
    
}
