<?php
require_once("../bootstrap.php");

if(isset($_POST["registration-email"], $_POST["encryptedPassword"],$_POST["first-name"],$_POST["last-name"])) { 
   
   //$dbResult = $db->changeUser("result","test2","password","salt","first_name","last_name");
   $dbResult = changeCredentials($_POST["registration-email"], $_POST["encryptedPassword"], $_POST["first-name"],$_POST["last-name"]);
   if($dbResult === SUCCESS) {
      login($_POST["registration-email"],$_POST["encryptedPassword"]);
      header("Location: ../successregistration.php?operation=profile&success=true");
      exit;
   } else {
      header("Location: ../profile.php?error=".$dbResult);
      exit;
   }
}
header("Location: ../profile.php?error=".EMPTY_FIELD);
?>

