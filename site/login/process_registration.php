<?php
require_once("../bootstrap.php");

if(isset($_POST["registration-email"], $_POST["encryptedPassword"],$_POST["first-name"],$_POST["last-name"])) { 
   $dbResult = registration($_POST["registration-email"], $_POST["encryptedPassword"], $_POST["first-name"],$_POST["last-name"]);
   if($dbResult == SUCCESS) {
      header("Location: ../successregistration.php?operation=registration&success=true");
      exit;
   } else {
      header("Location: ../register.php?error=".$dbResult);
      exit;
   }
}
header("Location: ../register.php?error=".EMPTY_FIELD);
?>
