<?php
require_once("../bootstrap.php");

if(isset($_POST["login-email"], $_POST["encryptedPassword"])) {
   $dbResult = login($_POST["login-email"], $_POST["encryptedPassword"]);
   if($dbResult == SUCCESS) {
      header("Location: ../index.php");
      exit;
   } else {
      header("Location: ../login.php?error=".$dbResult);
      exit;
   }
}else{
   header("Location: ../login.php?error=".EMPTY_FIELD);
} 
?>
