const x = "fa fa-times";
const heart = "fa fa-heart";
const cart = "fa fa-shopping-cart";
const edit = "fa fa-pencil";
const del = "fa fa-trash";
const eventForPageAPI ="utils/eventsForPagesAPI.php?";

function drawEvent(offset,limit,apply){
    if($("main.search").length){
        const result = $("main.search ul.results");
        events(result,"save","cart",heart,cart,"Search",offset,limit,apply,$("main.search strong").html());
        setTimeout(function(){
            if($("main.search div.container")){                
                $("main.search ul.results>li>p").empty();
            }
        },20);
    }
    if($("main.cart").length){
        events($("main.cart div.elements"),"remove","save",x,heart,"Cart",offset,limit,apply);
    }
    if($("main.save").length){
        events($("main.save div.elements"),"remove","cart",x,cart,"Save",offset,limit,apply);
    }
    if($("main.eventmanager").length){
        const owner = $("main.eventmanager select#owner").val();
        console.log(owner);
        switch(owner){
            case "Attended":
                events($("main.eventmanager section.events"),"remove","cart",x,cart,"Attended",offset,limit,apply);
            break;
            case "Created":
                events($("main.eventmanager section.events"),"delete","edit",del,edit,"Created",offset,limit,apply);
            break;
            case "Saved":
                events($("main.eventmanager section.events"),"remove","cart",x,cart,"Save",offset,limit,apply);
            break;
        }
    }
}
function events(resultSection,firstClass,secondClass,firstIcon,secondIcon,eventType,offset,limit,apply,search=""){
    $.getJSON(eventForPageAPI+"type="+eventType+"&offset="+offset+"&limit="+limit+"&value="+search,function(result){
        $.each(result,function(key,value){
            drawUser(resultSection,firstClass,secondClass,firstIcon,secondIcon,value,apply);
        });
        offset+=limit;
    });

}
function drawUser(resultSection,firstClass,secondClass,firstIcon,secondIcon,event,apply){
    const content = $(`<div class="container">
    <div class="image">
        <a href="preview.php?event_id=${event["event_id"]}">
            <img src="res/users/${event["user_id"]}/${event["image"]}" alt="" />
            </a>
        </div><!--
        --><div class="text">
            <h3>${event["title"]}</h3>
            <p>Author: ${event["first_name"]} ${event["last_name"]}</p>
            <p>${event["description"]}</p>
        </div>
        <div class="bottom">
            <div class="price">
                Price:
                <span class="event-price">${event["price"]}</span>
                €
            </div>
            <div class="buttons">
                <button class="${firstClass}"><strong class="${firstIcon}"></strong></button>
                <button class="${secondClass}"><strong class="${secondIcon}"></strong></button>
            </div>
        </div>
        <section class="hidden">
            <p class="event-id"> ${event["event_id"]}</p>
        </section>
    </div>`);
    apply(content);
    resultSection.append(content);

    content.find("button.remove").click(function(){
        remove($(this));
    });
    content.find("button.save").click(function(){
        if($("main.search").length){
            saveSearch($(this));
        }else{
            save($(this));
        }
    });
    content.find("button.cart").click(function(){
        if($("main.search").length){
            addToCartSearch($(this));
        }else{
            addToCart($(this));
        }
    });
    content.find("button.delete").click(function(){            
        deleteEvent($(this));
    });
    content.find("button.edit").click(function(){
        editEvent($(this));
    })
}
