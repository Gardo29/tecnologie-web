$(document).ready(function(){
    $("input#mobile-search-bar").keypress(function(key){
        if(key.which == 13){
            processSearch($(this).val());
        }
    });
    $("input#desktop-search-bar").keypress(function(key){
        if(key.which == 13){
            processSearch($(this).val());
        }
    });
    $("button#mobile-menu").click(function(){
        $("header section.mobile ul.menu").show();
        $("header section.mobile ul.nav-bar").hide();
    });
    $("ul.account-menu li").css("height",(100/$("ul.account-menu li").length)+"%");
    $("button#mobile-close-menu").click(function(){
        $("header section.mobile ul.menu").hide();
        $("header section.mobile ul.nav-bar").show();
    });
    $("button#mobile-search-button").click(function(){
        let searchBar = $("section.mobile li.search-bar-container");
        searchBar.stop(true,true);
        if($("section.mobile li.image-container").is(":hidden")){ 
            hideSearchBarMobile(searchBar,$("section.mobile li.image-container"));
        }else{
            switchClasses($("section.mobile li.menu-button-container"),"menu-button-container","button-container-hided");
            $("section.mobile li.image-container").hide();
            $("section.mobile li.cart-button-container").hide();
            searchBar.animate({width: 'toggle'});
            searchBar.css("display","inline-block");
            searchBar.children("input").focus();
        }
    });
    /*      desktop listeners      */
    $("button#desktop-search-button").click(function(){
        let searchBar = $("section.desktop li.search-bar-container");
        searchBar.stop(true,true);
        if(searchBar.is(":visible")){ 
            searchBar.animate({width: 'toggle'});
        }else{
            searchBar.animate({width: 'toggle'});
            searchBar.css("display","inline-block");
            searchBar.children("input").focus();
        }
    });
    $("section.desktop li.search-bar-container").focusout(function(){
        $("button#desktop-search-button").click();
    });
    $("section.mobile li.search-bar-container").focusout(function(){
        $("button#desktop-search-button").click();
    });
    checkNotifications();
});

function hideSearchBarMobile(searchBar,image){
    searchBar.animate({width: 'toggle'},function(){
        switchClasses($("li.button-container-hided"),"button-container-hided","menu-button-container");
        image.show();
        $("section.mobile li.cart-button-container").show();
    });
}
function switchClasses(item,firstClass,secondClass){
    item.removeClass(firstClass);
    item.addClass(secondClass);
}
function increaseCart(){
    $("body>header span.cart-number").each(function(){
        $(this).show();
        $(this).html(parseInt($(this).html())+1);
    });
}
function decreaseCart(){
    $("body>header span.cart-number").each(function(){
        const number = $(this).html();
        $(this).html(parseInt(number)-1);
        if(number - 1 == 0){
            $(this).hide();
        }
    });
}
function decreaseNotifications(){
    $("body>header span.notifications-number").each(function(){
        const number = $(this).html();
        $(this).html(parseInt(number)-1);
        if(number - 1 == 0){
            $(this).hide();
        }
    });
}
function checkNotifications(){
    $("body>header span.notifications-number").each(function(){
        const number = $(this).html();
        if(number == 0){
            $(this).hide();
        }
    });
    $("body>header span.cart-number").each(function(){
        const number = $(this).html();
        if(number == 0){
            $(this).hide();
        }
    });
}
function processSearch(value){
    if(value !== "" || value !== undefined){
        location.href = "search.php?value="+value;
    }
}

