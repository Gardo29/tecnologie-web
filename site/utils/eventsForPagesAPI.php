<?php
require_once("../bootstrap.php");
if(!isset($_GET["type"],$_GET["offset"],$_GET["limit"])){
    echo false;
    exit;
}
$result = array();
switch($_GET["type"]){
    case "Search":
        if(isset($_GET["value"])){
            $result = $db->searchEventFromString($_GET["value"],$_GET["offset"],$_GET["limit"]);
        }
        break;
    case "Cart":
        $cart = $db->getUserCart($_SESSION["user_id"],$_GET["offset"],$_GET["limit"]);
        foreach($cart as $singleEvent){
            array_push($result,$db->getEventById($singleEvent["event_id"])[0]);
        }
        break;
    case "Save":
        $save = $db->getMySaved($_SESSION["user_id"],$_GET["offset"],$_GET["limit"]);
        foreach($save as $singleEvent){
            array_push($result,$db->getEventById($singleEvent["event_id"])[0]);
        }
        break;
    case "Attended":
        $result = $db->getEventsIAttended($_SESSION["user_id"],$_GET["offset"],$_GET["limit"]);
        break;
    case "Created":
        $result = $db->getEventByAuthor($_SESSION["user_id"],$_GET["offset"],$_GET["limit"]);
        break;
}
header('Content-Type: application/json');
echo json_encode($result);
?>
