let offset = 0;
let limit = 5;
$(document).ready(function(){
    drawAndIncrease();
    $(window).bind('scroll', function() {
        if($(window).scrollTop() > $("main.search").offset().top + $("main.search").outerHeight() - window.innerHeight) {
            drawAndIncrease();
        }
    });
});

function drawAndIncrease(){
    drawEvent(offset,limit,function(){});
    offset+=limit;
}
