$(document).ready(function(){
    $("button#preferred").click(function(){
        const button = $(this);
        const cartButton = $("button#tickets");
        eventId = $("section p.event-id").html();
        if(button.hasClass("liked")){
            $.post("saved/removesave.php",{"event_id":eventId})
            .done(function(data){ 
                if(data === "removed"){
                    button.removeClass("liked");
                }
            });
        }
        else{
            $.post("cart/processsave.php",{"event_id":eventId})
            .done(function(data){ 
                if(data === "added"){
                    button.addClass("liked");
                    if(cartButton.html()==="Remove From Cart"){
                        cartButton.html("Add To Cart");
                        decreaseCart();
                    }
                }
            });
        }
    });
    $("button#tickets").click(function(){
        const button = $(this);
        const saveButton = $("button#preferred");
        eventId = $("section p.event-id").html();
        if(button.html()==="Add To Cart"){
            $.post("saved/processcart.php",{"event_id":eventId})
            .done(function(data){
                if(data === "added"){
                    console.log("increased");
                    button.html("Remove From Cart");
                    saveButton.removeClass("liked");
                    increaseCart();
                }
            });
        }
        else{
            $.post("cart/removecart.php",{"event_id":eventId})
            .done(function(data){
                if(data === "removed"){
                    button.html("Add To Cart");
                    decreaseCart();
                }
            });
        }
    });
});