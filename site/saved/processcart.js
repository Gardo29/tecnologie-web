function addToCart(button){
    eventId = button.parents("div.container").find("p.event-id").html();
    $.post("saved/processcart.php",{"event_id":eventId})
    .done(function(data){
        if(data === "added"){
            button.parent().parent().parent().slideUp(300);
            increaseCart();
        }
    });
}
function addToCartSearch(button){
    eventId = button.parents("div.container").find("p.event-id").html();
    $.post("saved/processcart.php",{"event_id":eventId})
    .done(function(data){
        if(data === "added"){
            increaseCart();
        }
        button.parent().parent().parent().slideUp(300);
    });
}
function remove(button){
    eventId = button.parents("div.container").find("p.event-id").html();
    $.post("saved/removesave.php",{"event_id":eventId})
    .done(function(data){
        if(data === "removed"){
            console.log("removed");
            button.parent().parent().parent().slideUp(300);
        }
    });
}