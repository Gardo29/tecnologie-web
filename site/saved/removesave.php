<?php 
require_once("../bootstrap.php");

if(!isset($_SESSION["user_id"],$_POST["event_id"])){
    echo false;
    exit;
}
$userId = $_SESSION["user_id"];
$eventId = $_POST["event_id"];
$dbResult = $db->deleteSavedEvent($eventId,$userId);
if(!$dbResult){
    echo false;
    exit;
}
echo "removed";
exit;
?>