<?php 
require_once("../bootstrap.php");

if(!isset($_SESSION["user_id"],$_POST["event_id"])){
    echo false;
    exit;
}
$userId = $_SESSION["user_id"];
$eventId = $_POST["event_id"];
$checkResult = $db->getIfInCart($eventId,$userId);
if($checkResult === DB_ERROR){
    echo DB_ERROR;
    exit;
}
if(!$checkResult){
    $dbResult = $db->createCartedEvent($eventId,$userId);
    if(!$dbResult){
        echo false;
        exit;
    }
    $dbResult = $db->deleteSavedEvent($eventId,$userId);
    if(!$dbResult){
        echo false;
        exit;
    }
    echo "added";
    exit;
}
echo false;
exit;
?>