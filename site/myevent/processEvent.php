<?php 
require_once("../bootstrap.php");
if(!loginCheck() || !isset($_POST["action"])){
    header("location: ../login.php");
}
if($_POST["action"]=="delete"){
    if(!isset($_POST["event_id"],$_SESSION["user_id"])){
        goBack("User informations are missing");
    }
    $userId = $_SESSION["user_id"];
    $eventId = $_POST["event_id"];
    //se c'e un'immagine nuova accettabile la salvo se no tengo la vecchia
    $creator = $db->getEventById($eventId)[0]["user_id"]; // se non sei il creatore ti sbatto fuori
    if($userId !== $creator){
        header("Location: ../index.php");
    }
    $delete = $db->deleteEvent($eventId);
    echo "deleted";
    exit;
}
if(!isset($_POST["title"],$_POST["address"],$_POST["city"],$_POST["start"],$_POST["start-time"],$_POST["start"],$_POST["start-time"],
          $_POST["description"],$_POST["tickets"],$_POST["price"],$_POST["category"]) 
          || (!isset($_POST["oldimg"]) && !isset($_FILES["event_image"]))){
    goBack("Empty fields");
}
foreach($_POST as $value){
    if($value === ""){
        goBack("Empty fields");
    }
}

$action = $_POST["action"];
$title = $_POST["title"];
$address = $_POST["address"];
$city = $_POST["city"];
$startDate = $_POST["start"];
$startTime = $_POST["start-time"];
$endDate = $_POST["start"];
$endTime = $_POST["start-time"];
$description = $_POST["description"];
$tickets = $_POST["tickets"];
$price = $_POST["price"];
$category = $_POST["category"];

$result = false;
if($action === "create"){
    if(!isset($_FILES["event_image"]) || strlen($_FILES["event_image"]["name"])<=0){
        goBack("Image Is Missing");
    }
    list($result, $msg) = uploadImage("../".USER_RES.$_SESSION["user_id"],$_FILES["event_image"]);
    if($result === false){
        goBack($msg);
    }
    $image = $msg;
    $db->createEvent($title, $description, $image, $startDate." ".$startTime, $endDate." ".$endTime, $category, $_SESSION["user_id"], $tickets, $price, $city, $address);
    goBack("Event Created Successfully!");
}
if($action === "modify"){
    
    //Controllo abbia un id e una immagine vecchia
    if(!isset($_POST["event_id"],$_POST["oldimg"],$_POST["notification"])){
        goBack("Image's missing");
    }
    $eventId = $_POST["event_id"];
    $userId = $_SESSION["user_id"];
    //se c'e un'immagine nuova accettabile la salvo se no tengo la vecchia
    $creator = $db->getEventById($eventId)[0]["user_id"]; // se non sei il creatore ti sbatto fuori
    if($userId !== $creator){
        header("Location: ../index.php");
    }
    if(isset($_FILES["event_image"]) && strlen($_FILES["event_image"]["name"])>0){
        list($result, $msg) = uploadImage("../".USER_RES.$userId."/",$_FILES["event_image"]);
        if($result === false){
            goBack($msg);
        }
        $image = $msg;
    }
    else{
        $image = $_POST["oldimg"];
    }
    $db->modifyEvent($eventId, $title, $description, $image,$startDate." ".$startTime, $endDate." ".$endTime, $category, $tickets, $price, $city, $address);
    $users = array_diff($db->getAllExternalEvent($eventId),[$userId]);
    foreach($users as $user){
        $notResult = $db->createNotification($title,$user,$_POST["notification"]);
    }
    goBack("Changes Made Successfully!");
}
/*      Functions       */
function uploadImage($path, $image){
    $imageName = basename($image["name"]);
    $fullPath = $path."/".$imageName;
    
    $maxKB = 1000; // 1000KB
    $acceptedExtensions = array("jpg", "jpeg", "png", "gif");
    $result = false;
    $msg = "";
    //Controllo se immagine è veramente un'immagine
    $imageSize = getimagesize($image["tmp_name"]);
    if($imageSize === false) {
        $msg .= "The file isnn't an image ";
    }
    //Controllo dimensione dell'immagine < 500KB
    if ($image["size"] > $maxKB * 1024) {
        $msg .= "The file size exceeds 500 kb allowed ";
    }

    //Controllo estensione del file
    $imageFileType = strtolower(pathinfo($fullPath,PATHINFO_EXTENSION));
    if(!in_array($imageFileType, $acceptedExtensions)){
        $msg .= "Only accept the following extensions: ".implode(",", $acceptedExtensions);
    }
    //Controllo se esiste file con stesso nome ed eventualmente lo rinomino
    if (file_exists($fullPath)) {
        $i = 1;
        do{
            $i++;
            $imageName = pathinfo(basename($image["name"]), PATHINFO_FILENAME)."_$i.".$imageFileType;
        }
        while(file_exists($path.$imageName));
        $fullPath = $path.$imageName;
    }
    if(!file_exists($path)) {
        if(!mkdir($path)){
            $msg.= "Error in folder creation ";
        }
    }
    //Se non ci sono errori, sposto il file dalla posizione temporanea alla cartella di destinazione
    if(strlen($msg)==0){
        if(!move_uploaded_file($image["tmp_name"], $fullPath)){
            $msg.= "Error in the image loading ";
        }
        else{
            $result = true;
            $msg = $imageName;
        }
    }
    return array($result, $msg);
}
function goBack($msg){
    if (isset($_SERVER["HTTP_REFERER"])) {
        header("Location: " . $_SERVER["HTTP_REFERER"]."&msg=".$msg);
        exit;
    }else{
        header("Location: ../eventsmanagement.php");
        exit;
    }
}
?>
