<?php
require_once("bootstrap.php");

//Base Template
$templateParams["mainclass"] = "login";
$templateParams["content"] = TEMPLATE."register.php";
$templateParams["title"] = "Register";
$templateParams["js"] = array(LOGIN_DIR."formUtility.js",LOGIN_DIR."submitForm.js",LOGIN_DIR."sha512.js");
$templateParams["php"] = LOGIN_DIR."process_registration.php";
$templateParams["button_text"] = "Register";
$templateParams["input"] = array("email"=>"","name"=>"","last_name"=>"");

require TEMPLATE.'/base.php';
?>
