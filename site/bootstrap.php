<?php
define("LOGIN_DIR", "login/");
define("UPLOAD_DIR", "res/");
define("USER_RES", "res/users/");
define("JS_DIR", "utils/");
define("PHP_DIR", JS_DIR);
define("EVENTS_MANAGEMENT",UPLOAD_DIR."events-management/");
define("MENU_RES",UPLOAD_DIR."menu-images/");
define("EVENT_RES",UPLOAD_DIR."event-images/");
define("TEMPLATE","template/");
define("DB_DIR","db/");
define("EVENT_LIST_DIR","myevent/");
define("EVENT_DIR","myevent/");
define("NOTIFICATIONS_DIR","notifications/");
define("ADMIN_DIR","admin/");
define("SAVED_DIR","saved/");
define("CART_DIR","cart/");

require_once(DB_DIR."database.php");
require_once(LOGIN_DIR."userManagementFunctions.php");
require_once(PHP_DIR."PHPUtils.php");
sec_session_start();

$categories = array("Music","Nature","Conference","Exhibition","Technology","Style","Spiritualiy","Free Time","Sport","Course");
    
if(loginCheck()){
    $cart = $db->getUserCart($_SESSION["user_id"]);
    $notifications = $db->getNotifications($_SESSION["user_id"]);
}else{
    $cart = array();
    $notifications = array();
}
?>
