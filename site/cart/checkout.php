<?php

require_once("../bootstrap.php");

if(!isset($_POST["quantity"],$_POST["event_id"])){
    echo "fail";
}
$quantity = $_POST["quantity"];
$eventId = $_POST["event_id"];
$available = $db->getAvailablePlaces($eventId);
if(!$available){
    echo "fail";
    exit;
}
$available = $available[0]["ticket_number"];
$difference = $available - $quantity;

if($difference>=0){
    $dbResult = $db->modifyTicketNumber($eventId,$difference);
    if(!$dbResult){
        echo "fail";
        exit;
    }
    $previus = $db->getTicket($eventId,$_SESSION["user_id"]);
    if(count($previus)>0){
        $dbResult = $db->modifyNumberOfTickets($eventId,$_SESSION["user_id"],$quantity+$previus[0]["number_of_tickets"]);
        echo $quantity+$previus[0]["number_of_tickets"];
        exit;
    }else{
        $dbResult = $db->createTicket($eventId,$_SESSION["user_id"],$quantity);
        if(!$dbResult){
            echo "fail creazione";
            exit;
        }
    }
    if(!$dbResult){
        echo "fail";
        exit;
    }
    echo "success";
    exit;
}
echo "fail";
exit;
?>