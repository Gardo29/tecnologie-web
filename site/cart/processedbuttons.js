
function save(button){
    eventId = button.parents("div.container").find("p.event-id").html();
    oldPrice = parseInt(button.parents("div.container").find("div.price span").html());
    $.post("cart/processsave.php",{"event_id":eventId})
    .done(function(data){
        if(data === "added"){
            decreaseCart();
            updateTotal(oldPrice, 0);
            button.parent().parent().parent().slideUp(300);
        }
    });
}
function saveSearch(button){
    eventId = button.parents("div.container").find("p.event-id").html();
    $.post("cart/processsave.php",{"event_id":eventId})
    .done(function(data){
        button.parent().parent().parent().slideUp(300);
    });
}
function remove(button){
    eventId = button.parents("div.container").find("p.event-id").html();
    oldPrice = parseInt(button.parents("div.container").find("div.price span").html()); 
    $.post("cart/removecart.php",{"event_id":eventId})
    .done(function(data){
        if(data === "removed"){
            decreaseCart();
            updateTotal(oldPrice, 0);
            button.parent().parent().parent().slideUp(300);
        }
    });
}
function deleteEvent(button){
    eventId = button.parents("div.container").find("p.event-id").html();
    $.post("myevent/processevent.php",{"action":"delete","event_id":eventId})
        .done(function(result){
            if(result == "deleted"){
                button.parents("div.container").slideUp(300);
            }
    });
}
function editEvent(button){
    eventId = button.parents("div.container").find("p.event-id").html();
    location.href="myevent.php?action=modify&event_id="+eventId;
}
