let offset = 0;
let limit = 5;
$(document).ready(function(){
    drawAndIncrease();
    $(window).bind('scroll', function() {
        if($(window).scrollTop() > $("main.cart").offset().top + $("main.cart").outerHeight() - window.innerHeight) {
            drawAndIncrease();
        }
    });
    $("button.cart-checkout").click(function(){
        checkout();
    });
});
function drawAndIncrease(){
    drawEvent(offset,limit,function(event){
        drawSection(event);
    });
    offset+=limit;
}

function drawSection(event){
    event.find("div.price").append(
        $("<div class=\"quantity\"></div>")
        .append($(`<button class="less"><strong class="fa fa-minus"></strong</button></button>`).click(function(){
        modifyQuantity(event,"decrease");
    }))
    .append($(`<div class="current">1</div>`))
    .append($(`<button class="more"><strong class="fa fa-plus"></strong></button>`).click(function(){
        modifyQuantity(event,"increase");
    })));
}

function modifyQuantity(eventBlock,type){
    const quantity = eventBlock.find("div.current");
    let quantityNumber = parseInt(quantity.html());
    const singlePrice = calculateSinglePrice(eventBlock,quantityNumber);
    const oldPrice = quantityNumber*singlePrice;

    if(type === "increase"){
        quantityNumber++;
    }
    if(type === "decrease"){
        if(quantityNumber === 1){
            return;
        }
        quantityNumber--;
    }
    quantity.html(quantityNumber);
    newPrice = quantityNumber*singlePrice; 
    eventBlock.find("span").html(newPrice);   
    updateTotal(oldPrice,newPrice);
}

function updateTotal(oldPrice, newPrice){
    const totPrice = $("main.cart div.tot-price");
    let tot = parseInt(totPrice.html());
    totPrice.html(tot-oldPrice+newPrice);
}

function calculateSinglePrice(eventBlock,quantity){
    price = eventBlock.find("span").html();    
    return parseInt(price)/quantity;
}

function checkout(){
    let result = new Array();
    const events = $("div.container");
    const length = events.length;
    for(let i=0;i<length;i++){
        const eventId = $(events[i]).find("p.event-id").html();
        const quantity = $(events[i]).find("div.current").html();
        const title = $(events[i]).find("h3").html();
        $.post("cart/checkout.php",{"event_id":eventId,"quantity":quantity})
        .done(function(data){
            console.log(data);
            if(data === "fail"){
                result.push(title);
                proceed(i,length,result);
                return;
            }          
            $.post("cart/removecart.php",{"event_id":eventId})
            .done(function(data){
                if(data !== "removed"){ 
                    result.push(title);
                    proceed(i,length,result);
                    return;
                }
                proceed(i,length,result);
            });
        });
    }
}
function proceed(index,length,result){
    if(index == length-1 && result.length){
        result="?unpurchased="+result.join(",");
    }
    location.href="cartresult.php"+result;
}
