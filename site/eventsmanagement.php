<?php
require_once("bootstrap.php");

if(loginCheck() == false){
    header("Location: login.php");
    exit;
}
//Base Template
$templateParams["mainclass"] = "eventmanager";
$templateParams["content"] = TEMPLATE."eventmanagerbase.php";
$templateParams["title"] = "Event Manager";
$templateParams["js"] = array(JS_DIR."drawEvent.js",CART_DIR."processedbuttons.js",SAVED_DIR."processcart.js","eventsmanagement/manageEvents.js");
$templateParams["event-owner"] = array("Attended","Created","Saved");

require TEMPLATE.'/base.php';
?>
