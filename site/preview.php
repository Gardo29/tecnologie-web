<?php
require_once("bootstrap.php");

/*if(!loginCheck()){
    header("Location: index.php");
    exit;
}*/

if(isset($_GET["event_id"])){
    $event = $db->getEventById($_GET["event_id"]);
    if(!$event || count($event) == 0){
        header("Location: index.php");
    }
    $event = $event[0];
}else{
    header("Location: index.php");
    exit;
}

$event["image"] = USER_RES.$event["user_id"]."/".$event["image"];

//Base Template
$templateParams["mainclass"] = "preview";
$templateParams["content"] = "previewtemplate.php";
$templateParams["title"] = "Preview";
$templateParams["js"] = array(JS_DIR."managePreview.js");
$templateParams["active"] = $event["start_date_time"] >= date("Y-m-d H:i:s") ? true : false;

if(loginCheck()){
    $templateParams["button"] = $db->getIfInCart($_GET["event_id"],$_SESSION["user_id"]) ? "Remove From Cart" : "Add To Cart";
    $templateParams["liked"] = $db->getIfSaved($_GET["event_id"],$_SESSION["user_id"]);

}else{
    $templateParams["button"] = "Login to add to cart";
}

require TEMPLATE.'/base.php';
?>
