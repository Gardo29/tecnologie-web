<?php
require_once("bootstrap.php");

if(loginCheck() == false){
    header("Location: login.php");
    exit;
}
$dbResult = $db->searchUser($_SESSION["user_id"]);
if(!$dbResult || count($dbResult) == 0){
    header("Location: login.php");
    exit;
}
$user = $dbResult[0];
//Base Template
$templateParams["mainclass"] = "login profile";
$templateParams["content"] = "accounttemplate.php";
$templateParams["title"] = "Account Settings";
$templateParams["js"] = array(LOGIN_DIR."formUtility.js",LOGIN_DIR."submitForm.js",LOGIN_DIR."sha512.js");
$templateParams["php"] = LOGIN_DIR."changeCredentials.php";
$templateParams["button_text"] = "Save";
$templateParams["input"] = array("email"=>$user["email"],"name"=>$user["first_name"],"last_name"=>$user["last_name"]);

require TEMPLATE.'/base.php';
?>
