$(document).ready(function(){

    $("main.notifications button").click(function(){
        deleteNotification($(this).parents("main.notifications>ul>li"));
    });
    $("main.notifications>ul>li").swiperight(function(){
        deleteNotification($(this));
    });
});

function deleteNotification(notifications){
    const userId = notifications.find("li.user-id").html();
    const notId = notifications.find("li.notification-id").html();
    console.log(userId);
    $.post("notifications/removeNotification.php",{"user_id":userId,"notification_id":notId})
    .done(function(data){
        if(data == true){
            $(notifications).toggle(300);
            decreaseNotifications();
        }
    });
}

