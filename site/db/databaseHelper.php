<?php
class DatabaseHelper{
    private $db;

    public function __construct($servername, $username, $password, $dbname){
        $this->db = new mysqli($servername, $username, $password, $dbname);
        if ($this->db->connect_error) {
            die("Connection failed: " . $db->connect_error);
        }        
    }
    public function searchUser($email){
        $stmt = $this->db->prepare("SELECT email, password_cript as password, salt, admin,is_active,first_name,last_name FROM user WHERE email = ? LIMIT 1");
        if($stmt){
            $stmt->bind_param('s', $email); // esegue il bind del parametro '$email'.
            $stmt->execute(); // esegue la query appena creata.
            return $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
        }
        return false;
    }
    public function changeUser($old_email,$email,$password,$salt,$first_name,$last_name){
        $stmt = $this->db->prepare("UPDATE user SET email=?,last_update=NOW(),password_cript=?,salt=?,first_name=?,last_name=? WHERE email=?");
        if($stmt){
            $stmt->bind_param('ssssss', $email,$password,$salt,$first_name,$last_name,$old_email); // esegue il bind del parametro '$email'.
            $stmt->execute(); // esegue la query appena creata.
            return true;
        }
        return false;
    }
    /* my functions */
    public function registerUser($email,$password,$name,$surname,$salt){
        $stmt = $this->db->prepare("INSERT INTO user (email,first_name,last_name,password_cript,salt,date_of_subscription,last_update,is_active,admin)
                                                VALUES (?,?,?,?,?,NOW(),NOW(),?,?)");
        if($stmt){
            $active = true;
            $admin = false;
            $stmt->bind_param("sssssii",$email,$name,$surname,$password,$salt,$active,$admin);
            $stmt->execute();
            return true;
        }
        return false;
    }

    /* EVENT  */
    //get available tickets
    public function getAvailablePlaces($event_id){
        $query = "SELECT ticket_number FROM event e WHERE e.event_id=?";
        $stmt = $this->db->prepare($query);
        if($stmt){
            $stmt->bind_param('i',$event_id);
            $stmt->execute();
            $result = $stmt->get_result();
            return $result->fetch_all(MYSQLI_ASSOC);
        }
        return false;
    }
    //by event_id
    public function getEventById($event_id){
        $query = "SELECT e.event_id,e.user_id, title,ticket_number, image, description, start_date_time, end_date_time, e.price, first_name, last_name, city,address,category FROM event e, user u, location l WHERE e.event_id=? AND e.user_id=u.email AND e.event_id=l.event_id";
        $stmt = $this->db->prepare($query);
        if($stmt){
            $stmt->bind_param('i',$event_id);
            $stmt->execute();
            $result = $stmt->get_result();
            return $result->fetch_all(MYSQLI_ASSOC);
        }
        return false;
    }
    //by author
    public function getEventByAuthor($author,$offset=-1,$limit=-1){
        $query = "SELECT e.event_id,e.user_id,title, image, description, start_date_time, end_date_time, e.price, first_name, last_name, city, address FROM event e, user u, location l WHERE u.email=? AND e.user_id=u.email AND e.event_id=l.event_id  ORDER BY start_date_time DESC";
        if($offset >= 0 && $limit >= 0){
            $query = $query." LIMIT ".$offset.",".($limit+$offset)." ";
        }
        $stmt = $this->db->prepare($query);
        if($stmt){
            $stmt->bind_param('s',$author);
            $stmt->execute();
            $result = $stmt->get_result();
            return $result->fetch_all(MYSQLI_ASSOC);
        }
        return false;
    }
    //by title
    public function getEventByTitle($title){
        $query = "SELECT e.event_id,title, image, description, start_date_time, end_date_time, e.price, first_name, last_name, city, address FROM event e, user u, location l WHERE e.title=? AND e.user_id=u.email AND e.event_id=l.event_id";
        $stmt = $this->db->prepare($query);
        if($stmt){
            $stmt->bind_param('s',$title);
            $stmt->execute();
            $result = $stmt->get_result();
            return $result->fetch_all(MYSQLI_ASSOC);
        }
        return false;
    }
    //by city
    public function getEventByCity($city){
        $query = "SELECT e.event_id,title, image, description, start_date_time, end_date_time, e.price, first_name, last_name, city, address FROM event e, user u, location l WHERE city=? AND e.user_id=u.email AND l.event_id=e.event_id";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s',$city);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }

    //by category
    public function getEventByCategory($category,$offset,$number){
        $query = "SELECT e.event_id,title, image, description, start_date_time, end_date_time, e.price, first_name, last_name, city, address FROM event e, user u, location l WHERE category=? AND e.user_id=u.email AND e.event_id=l.event_id LIMIT ?,?";
        $stmt = $this->db->prepare($query);
        if($stmt){
            $stmt->bind_param('sii',$category,$offset,$number);
            $stmt->execute();
            $result = $stmt->get_result();
            return $result->fetch_all(MYSQLI_ASSOC);
        }
        return false;
    }
    //by category not expired
    public function getAvailableEventByCategory($category,$offset,$number){
        $query = "SELECT e.event_id,title, image, description, start_date_time, end_date_time, e.price, first_name, last_name, e.user_id, city, address FROM event e, user u, location l WHERE category=? AND end_date_time > NOW() AND e.user_id=u.email AND e.event_id=l.event_id LIMIT ?,?";
        $stmt = $this->db->prepare($query);
        if($stmt){
            $stmt->bind_param('sii',$category,$offset,$number);
            $stmt->execute();
            $result = $stmt->get_result();
            return $result->fetch_all(MYSQLI_ASSOC);
        }
        return false;
    }
    //my past created
    public function getMyPastEvents($author,$offset,$number){
        $query = "SELECT e.event_id,title, image, description, start_date_time, end_date_time, e.price, first_name, last_name, city, address FROM event e, user u, location l WHERE u.email=? AND end_date_time < NOW() AND e.user_id=u.email AND e.event_id=l.event_id LIMIT ?,?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('sii',$author,$offset,$number);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC);
    }
    
    //my present created
    public function getMyPresentEvent($author,$offset,$number){
        $query = "SELECT e.event_id,title, image, description, start_date_time, end_date_time, e.price, first_name, last_name, city, address FROM event e, user, location l WHERE email=? AND end_date_time > NOW() AND e.user_id=email AND e.event_id=l.event_id LIMIT ?,?";
        $stmt = $this->db->prepare($query);
        if($stmt){
            $stmt->bind_param('sii',$author,$offset,$number);
            $stmt->execute();
            return $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
        }
        return false;
    }
    
    public function getEventsIAttended($user,$offset,$number){
        $query = "SELECT e.event_id,e.user_id, title, city, image, description, start_date_time, end_date_time, e.price, first_name, last_name, city, address,t.number_of_tickets FROM event e, user u, ticket t, location l WHERE t.user_id=? AND e.user_id=u.email  AND e.event_id=t.event_id AND e.event_id=l.event_id LIMIT ?,?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('sii',$user,$offset,$number);
        $stmt->execute();
        $result = $stmt->get_result();
        
        return $result->fetch_all(MYSQLI_ASSOC);
    }    
    
    //event insertion
    public function createEvent($title, $description, $image, $start_date_time, $end_date_time, $category, $user_id, $ticket_number, $price, $city, $address){
        $stmt = $this->db->prepare("INSERT INTO event ( title, description, image, start_date_time, end_date_time, date_of_creation, last_update, category, user_id, ticket_number, price)
                                                VALUES (?,?,?,?,?,NOW(),NOW(),?,?,?,?)");
       if(!$stmt){
           return false;
        }
        $stmt->bind_param("sssssssii",$title, $description, $image, $start_date_time, $end_date_time, $category, $user_id, $ticket_number, $price);
        $stmt->execute();
        $stmt = $this->db->prepare("SELECT MAX(event_id) as event_id FROM event");
        $stmt->execute();
        $event_id = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
        $stmt = $this->db->prepare("INSERT INTO location (event_id, city, address)
                                                VALUES (?,?,?)");
        if($stmt){
            $stmt->bind_param("iss",$event_id[0]["event_id"],$city,$address);
            $stmt->execute();
            return true;
        }
        return false;
    } 
    //modify event
    public function modifyEvent($event_id, $title, $description, $image, $start_date_time, $end_date_time, $category, $ticket_number, $price, $city, $address){
        $stmt = $this->db->prepare("UPDATE event SET title=?, description=?, image=?, start_date_time=?, end_date_time=?, last_update=NOW(), category=?, ticket_number=?, price=? WHERE event_id=?");
       if(!$stmt){
           return false;
        }
        $stmt->bind_param("ssssssiii",$title, $description, $image, $start_date_time, $end_date_time, $category, $ticket_number, $price, $event_id);
        $stmt->execute();
        $stmt = $this->db->prepare("UPDATE location SET city=?, address=? WHERE event_id=?");
        if($stmt){
            $stmt->bind_param("ssi",$city,$address,$event_id);
            $stmt->execute();
            return true;
        }
        return false;
    }
    //modify available places
    public function modifyTicketNumber($event_id, $newTicketNumber){
        $stmt = $this->db->prepare("UPDATE event SET ticket_number=? WHERE event_id=?");
        if($stmt){
            $stmt->bind_param("ii",$newTicketNumber,$event_id);
            $stmt->execute();
            return true;
        }
        return false;
    }
    public function getTickesByEvent($event_id){
        $stmt = $this->db->prepare("SELECT * FROM ticket WHERE event_id=?");
        if($stmt){
            $stmt->bind_param("i",$event_id);
            $stmt->execute();
            return $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
        }
        return false;
    }
    public function getTicket($event_id,$user_id){
        $stmt = $this->db->prepare("SELECT * from ticket WHERE user_id = ? AND event_id = ?");
        if($stmt){
            $stmt->bind_param("si",$user_id,$event_id);
            $stmt->execute();
            return $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
        }
        return false;
    }
    public function modifyNumberOfTickets($event_id,$user_id,$newNumber){
        $stmt = $this->db->prepare("UPDATE ticket SET number_of_tickets = ? WHERE user_id = ? AND event_id = ?");
        if($stmt){
            $stmt->bind_param("isi",$newNumber,$user_id,$event_id);
            $stmt->execute();
            return true;
        }
        return false;
    }
    public function getCartByEvent($event_id){
        $stmt = $this->db->prepare("SELECT * FROM cart WHERE event_id=?");
        if($stmt){
            $stmt->bind_param("i",$event_id);
            $stmt->execute();
            return $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
        }
        return false;
    }
    public function getSavedByEvent($event_id){
        $stmt = $this->db->prepare("SELECT * FROM saved_event WHERE event_id=?");
        if($stmt){
            $stmt->bind_param("i",$event_id);
            $stmt->execute();
            return $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
        }
        return false;
    }
    //delete event
    public function deleteEvent($event_id){
        $stmt = $this->db->prepare("DELETE FROM event WHERE event_id=?");
        if($stmt){
            $stmt->bind_param("i",$event_id);
            $stmt->execute();
            return true;
        }
        return false;
    } 
    /**LOCATION */
    public function deleteLocation($event_id){
        $stmt = $this->db->prepare("DELETE FROM location WHERE event_id=?");  
        if($stmt){
            $stmt->bind_param("i",$event_id);
            $stmt->execute();
            return true;
        }
        return false;
    
    }
    /** TICKET */
    public function createTicket($event_id, $user_id,$quantity){
        $stmt = $this->db->prepare("INSERT INTO ticket (event_id, user_id,number_of_tickets)
                                                VALUES (?,?,?)");
        if($stmt){
            $stmt->bind_param("isi",$event_id, $user_id,$quantity);
            $stmt->execute();
            return true;
        }
        return false;
    } 
    //delete all tickets of one event
    public function deleteAllTickets($event_id){
        $stmt = $this->db->prepare("DELETE FROM ticket WHERE event_id=?");  
        if($stmt){
            $stmt->bind_param("i",$event_id);
            $stmt->execute();
            return true;
        }
        return false;
    } 
    //delete my ticket
    public function deleteTicket($event_id, $user_id){
        $stmt = $this->db->prepare("DELETE FROM ticket WHERE event_id=? AND user_id=?");  
        if($stmt){
            $stmt->bind_param("is",$event_id, $user_id);
            $stmt->execute();
            return true;
        }
        return false;
    } 
    
    /**NOTIFICATION */
    public function createNotification($event_title, $user_id,$description){
        $stmt = $this->db->prepare("INSERT INTO notification ( time_of_notification, event_title, user_id,description)
                                                VALUES (NOW(),?,?,?)");
        if($stmt){
            $stmt->bind_param("sss", $event_title, $user_id,$description);
            $stmt->execute();
            return true;
        }
        return false;
    } 
    public function getNotifications($user_id){
        $stmt = $this->db->prepare("SELECT * FROM notification where user_id = ?");
        if($stmt){
            $stmt->bind_param("s",$user_id);
            $stmt->execute();

            return $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
        }
        return false;
    }
    public function removeNotification($notification_id,$user_id){
        $stmt = $this->db->prepare("DELETE FROM notification where notification_id = ? AND user_id = ?");
        if($stmt){
            $stmt->bind_param("is",$notification_id,$user_id);
            $stmt->execute();
            return true;
        }
        return false;
    }
    public function getAllExternalEvent($event_id){
        $tickets = array_column($this->getTickesByEvent($event_id),"user_id");
        $cart = array_column($this->getCartByEvent($event_id),"user_id");
        $saved = array_column($this->getSavedByEvent($event_id),"user_id");
        return array_unique(array_merge($saved,array_merge($tickets,$cart)));
    }

    /** SAVED EVENT*/ 
    public function createSavedEvent($event_id, $user_id){
        $stmt = $this->db->prepare("INSERT INTO saved_event (event_id, user_id) VALUES (?,?)");
        if($stmt){
            $stmt->bind_param("is",$event_id,$user_id);
            $stmt->execute();
            return true;
        }
        return false;
    } 
    public function getIfSaved($event_id, $user_id){
        $stmt = $this->db->prepare("SELECT * FROM saved_event WHERE user_id = ? AND event_id = ?");
        if($stmt){
            $stmt->bind_param("si",$user_id,$event_id);
            $stmt->execute();
            $result = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
            if(count($result)){
                return true;
            }
            return false;
        }
        return DB_ERROR;
    } 

    public function getMySaved($user_id,$offset=-1,$limit=-1){
        $query = "SELECT event_id FROM saved_event WHERE user_id = ?";
        if($offset >= 0 && $limit > 0){
            $query = $query." LIMIT ".$offset.",".($limit+$offset)." ";
        }
        $stmt = $this->db->prepare($query);
        if($stmt){
            $stmt->bind_param("s",$user_id);
            $stmt->execute();
            $result = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
            if(count($result)){
                return $result;
            }
            return false;
        }
        return DB_ERROR;
    } 
    //delete saved event
    public function deleteSavedEvent($event_id, $user_id){
        $stmt = $this->db->prepare("DELETE FROM saved_event WHERE event_id=? AND user_id=?");  
        if($stmt){
            $stmt->bind_param("is",$event_id, $user_id);
            $stmt->execute();
            return true;
        }
        return false;
    }
    //delete event from all saved events 
    public function deleteEventFromAllSavedEvent($event_id){
        $stmt = $this->db->prepare("DELETE FROM saved_event WHERE event_id=?");  
        if($stmt){
            $stmt->bind_param("i",$event_id);
            $stmt->execute();
            return true;
        }
        return false;
    }

    /** CARTED EVENT*/ 
    public function createCartedEvent($event_id, $user_id){
        $stmt = $this->db->prepare("INSERT INTO cart (event_id, user_id) VALUES (?,?)");
        if($stmt){
            $stmt->bind_param("is",$event_id,$user_id);
            $stmt->execute();
            return true;
        }
        return false;
    } 
    public function getUserCart($user_id,$offset=-1,$limit=-1){
        $query = "SELECT * FROM cart WHERE user_id = ?";
        if($offset >= 0 && $limit >=0){
            $query = $query." LIMIT ".$offset.",".($limit+$offset)." ";
        }
        $stmt = $this->db->prepare($query);
        if($stmt){
            $stmt->bind_param("s",$user_id);
            $stmt->execute();
            return $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
        }
        return false;
    }
    public function getIfInCart($event_id, $user_id){
        $stmt = $this->db->prepare("SELECT * FROM cart WHERE user_id = ? AND event_id = ?");
        if($stmt){
            $stmt->bind_param("si",$user_id,$event_id);
            $stmt->execute();
            $result = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
            if(count($result)){
                return true;
            }
            return false;
        }
        return DB_ERROR;
    }
    //delete single event in cart
    public function deleteSingleEventInCart($event_id, $user_id){
        $stmt = $this->db->prepare("DELETE FROM cart WHERE event_id=? AND user_id=?");  
        if($stmt){
            $stmt->bind_param("is",$event_id, $user_id);
            $stmt->execute();
            return true;
        }
        return false;
    }
    //delete cart
    public function deleteCart($user_id){
        $stmt = $this->db->prepare("DELETE FROM cart WHERE user_id=?");  
        if($stmt){
            $stmt->bind_param("s",$user_id);
            $stmt->execute();
            return true;
        }
        return false;
    }
    //delete event in all carts
    public function deleteEventInAllCarts($event_id){
        $stmt = $this->db->prepare("DELETE FROM cart WHERE event_id=?");  
        if($stmt){
            $stmt->bind_param("i",$event_id);
            $stmt->execute();
            return true;
        }
        return false;
    }

    /*LOGIN ATTEMPTS */
    public function createNewLoginAttempt($user_id){
        $stmt = $this->db->prepare("INSERT INTO login_attempts (user_id, time) VALUES (?,NOW())");
        if($stmt){
            $stmt->bind_param("s", $user_id);
            $stmt->execute();
            return true;
        }
        return false;
    } 
    public function setUserState($userId,$state){
        $stmt = $this->db->prepare("UPDATE user SET is_active=?,last_update=NOW() WHERE email=?");
        if($stmt){
            $stmt->bind_param("is",$state,$userId);
            $stmt->execute();
            return true;
        }
        return false;
    }
    public function setAdmin($userId,$admin){
        $stmt = $this->db->prepare("UPDATE user SET admin=?,last_update=NOW() WHERE email=?");
        if($stmt){
            $stmt->bind_param("is",$admin,$userId);
            $stmt->execute();
            return true;
        }
        return false;
    }
    public function getLoginByTime($user_id,$time){
        $stmt = $this->db->prepare("SELECT * FROM login_attempts WHERE user_id = ? and time > ? ");
        if($stmt){
            $stmt->bind_param('si',$user_id,$time);
            $stmt->execute();
            return $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
        }
        return false;
    }
    public function removeAttempts($user_id){
        $stmt = $this->db->prepare("DELETE FROM login_attempts WHERE user_id = ?");
        if($stmt){
            $stmt->bind_param("s", $user_id);
            $stmt->execute();
            return true;
        }
        return false;
    }
    /* search engine */
    public function searchUserFromString($string){
        $stmt = $this->db->prepare("SELECT email,is_active FROM user WHERE email LIKE '%".$string."%' OR first_name LIKE '%".$string."%' OR last_name LIKE '%".$string."%'");
        if($stmt){
            $stmt->execute();
            return $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
        }
        return false;
    }
    public function searchEventFromString($string,$offset=-1,$limit=-1){
        $query = "SELECT e.event_id, e.user_id, title, image, description, start_date_time, end_date_time, price, first_name, last_name, city, address FROM event e, user u, location l WHERE e.user_id=u.email AND e.event_id=l.event_id AND e.start_date_time > NOW() AND ( e.event_id LIKE '%".$string."%' OR description LIKE '%".$string."%' OR title LIKE '%".$string."%' OR category LIKE '%".$string."%' OR city LIKE '%".$string."%' OR address LIKE '%".$string."%')";
        if($limit >= 0 && $offset >= 0){
            $query = $query." LIMIT ".$offset.",".($limit+$offset)." ";
        }
        $stmt = $this->db->prepare($query);
        if($stmt){
            $stmt->execute();
            return $stmt->get_result()->fetch_all(MYSQLI_ASSOC);
        }
        return false;
    }
}
?>
