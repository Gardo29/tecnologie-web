-- *********************************************
-- * SQL MySQL generation                      
-- *--------------------------------------------
-- * DB-MAIN version: 11.0.1              
-- * Generator date: Dec  4 2018              
-- * Generation date: Mon Jan 13 17:58:13 2020 
-- * LUN file: C:\Users\F541UVXX146T\Desktop\tec web\progetto\site\db\Eventi.lun 
-- * Schema: LOGIC/1 
-- ********************************************* 


-- Database Section
-- ________________ 

create database LOGIC;
use LOGIC;


-- Tables Section
-- _____________ 

create table CART (
     user_id varchar(30) not null,
     event_id int not null,
     quantity int not null,
     constraint IDCART primary key (user_id, event_id));

create table EVENT (
     event_id int not null AUTO_INCREMENT,
     title varchar(200) not null,
     description varchar(1000) not null,
     image varchar(100) not null,
     start_date_time DATETIME not null,
     end_date_time DATETIME not null,
     date_of_creation DATETIME not null,
     last_update DATETIME not null,
     category varchar(15) not null,
     user_id varchar(30) not null,
     ticket_number int not null,
     price int not null,
     constraint IDEVENT_ID primary key (event_id));

create table LOCATION (
     event_id int not null,
     location_id int not null AUTO_INCREMENT,
     city varchar(50) not null,
     address varchar(50) not null,
     constraint FKEVE_loc_ID unique (event_id),
     constraint IDLOCATION primary key (location_id));

create table LOGIN_ATTEMPTS (
     user_id varchar(30) not null,
     time DATETIME not null,
     constraint IDLOGIN_ATTEMPTS primary key (user_id, time));

create table NOTIFICATION (
     notification_id int not null AUTO_INCREMENT,
     time_of_notification DATETIME not null,
     description varchar(50) not null,
     event_title varchar(20) not null,
     user_id varchar(30) not null,
     constraint IDNOTIFICATION primary key (notification_id) );

create table SAVED_EVENT (
     user_id varchar(30) not null,
     event_id int not null,
     constraint IDSAVED_EVENT primary key (user_id, event_id));

create table TICKET (
     user_id varchar(30) not null,
     event_id int not null,
     number_of_tickets int not null,
     constraint IDTICKET primary key (user_id, event_id));

create table USER (
     email varchar(30) not null,
     first_name varchar(15) not null,
     last_name varchar(15) not null,
     password_cript varchar(128) not null,
     salt varchar(128) not null,
     date_of_subscription DATETIME not null,
     last_update DATETIME not null,
     is_active char not null,
     admin char not null,
     constraint IDUSER_ID primary key (email));


-- Constraints Section
-- ___________________ 

alter table CART add constraint FKCAR_EVE
     foreign key (event_id)
     references EVENT (event_id)
     ON UPDATE CASCADE
     ON DELETE CASCADE;
     
alter table CART add constraint FKCAR_USE
     foreign key (user_id)
     references USER (email)
     ON UPDATE CASCADE
     ON DELETE CASCADE;

-- Not implemented
-- alter table EVENT add constraint IDEVENT_CHK
--     check(exists(select * from TICKET
--                  where TICKET.event_id = event_id)); 

-- Not implemented
-- alter table EVENT add constraint IDEVENT_CHK
--     check(exists(select * from LOCATION
--                  where LOCATION.event_id = event_id)); 

alter table EVENT add constraint FKCREATION
     foreign key (user_id)
     references USER (email)
     ON UPDATE CASCADE
     ON DELETE CASCADE;

alter table LOCATION add constraint FKEVE_loc_FK
     foreign key (event_id)
     references EVENT (event_id)
     ON UPDATE CASCADE
     ON DELETE CASCADE;

alter table LOGIN_ATTEMPTS add constraint FKLOGIN
     foreign key (user_id)
     references USER (email)
     ON UPDATE CASCADE
     ON DELETE CASCADE;

alter table NOTIFICATION add constraint FKCOMPOSITION
     foreign key (user_id)
     references USER (email)
     ON UPDATE CASCADE
     ON DELETE CASCADE;

alter table SAVED_EVENT add constraint FKSAV_EVE
     foreign key (event_id)
     references EVENT (event_id)
     ON UPDATE CASCADE
     ON DELETE CASCADE;

alter table SAVED_EVENT add constraint FKSAV_USE
     foreign key (user_id)
     references USER (email)
     ON UPDATE CASCADE
     ON DELETE CASCADE;

alter table TICKET add constraint FKTIC_EVE
     foreign key (event_id)
     references EVENT (event_id)
     ON UPDATE CASCADE
     ON DELETE CASCADE;

alter table TICKET add constraint FKTIC_USE
     foreign key (user_id)
     references USER (email)
     ON UPDATE CASCADE
     ON DELETE CASCADE;

-- Not implemented
-- alter table USER add constraint IDUSER_CHK
--     check(exists(select * from EVENT
--                  where EVENT.user_id = email)); 


-- Index Section
-- _____________ 

