<?php 
require_once("databaseHelper.php");
define("HOST", "localhost"); // E' il server a cui ti vuoi connettere.
define("USER", "root"); // E' l'utente con cui ti collegherai al DB.
define("PASSWORD", ""); // Password di accesso al DB.
define("DATABASE", "logic"); // Nome del database.

$db = new databaseHelper(HOST, USER, PASSWORD, DATABASE);

?>
