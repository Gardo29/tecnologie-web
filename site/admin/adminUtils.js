const adminAPI = "admin/adminAPI.php?option=";
const lock = `<strong class="fa fa-lock"></strong>`;
const unlock = `<strong class="fa fa-unlock-alt"></strong>`;
usersList = [];

$(document).ready(function(){
    $("main.admin input").keypress(function(key){
        if(key.which == 13){
            $("button#go").click();
        }
    });
    $("button#go").click(function(){
        const option = $("main.admin select").val();
        const value = $("input#search").val();
        const resultSection = $("ul.results");
        if(value === undefined || value === ""){
            return;
        }
        resultSection.html("");
        if(option === "Events"){
            events(resultSection,value);
        }
        else if(option === "Users"){
            users(resultSection,value);
        }
    });
});
function users(resultSection,value){
    $.getJSON(adminAPI+"users&value="+value,function(result){
        $.each(result,function(key,value){
            drawUser(resultSection,value);
        });
    });
}
function drawUser(resultSection,user){
    const icon = user["is_active"] == true ? unlock : lock;
    usersList[user["email"]] = user["is_active"];
    resultSection
    .append($("<li/>")
        .append($("<ul/>")
            .append($(`<li class="user">${user["email"]}</li>`))
            .append($("<li class=\"button\"></li>")
                .append($("<button>"+icon+"</button>").click(function(){
                    changeUserState($(this),user["email"]);
                })))));
}
function changeUserState(button,user){
    $.post("admin/userStateAPI.php",{"user":user,"state":usersList[user]}).done(function(result){
        if(result === "false"){
            return;
        }
        usersList[user] = usersList[user] == 1 ? 0 : 1;
        const icon = usersList[user] ? unlock : lock;
        button.html(icon);
    });
}
function events(resultSection,value){
    $.getJSON(adminAPI+"events&value="+value,function(result){
        $.each(result,function(key,value){
            drawEvents(resultSection,value);
        });
    });
}
function drawEvents(resultSection,event){
    resultSection
    .append($("<li/>")
            .append($("<ul/>")
                .append($(`<li class="user"><a href=\"preview.php?event_id=${event["event_id"]}\">${event["title"]}</br>${event["user_id"]}</a></li>`))
                .append($("<li class=\"button\"></li>")
                    .append($("<button><strong class=\"fa fa-times menu-icon\"></strong></button>").click(function(){
                        deleteEvent($(this),event["event_id"]);
                    })))));
}
function deleteEvent(button,event){
    $.post("admin/processEventDeletionAPI.php",{"event_id":event}).done(function(result){
        if(result === "false"){
            return;
        }
        button.parents("main.admin>ul>li").slideUp();
    });
}
