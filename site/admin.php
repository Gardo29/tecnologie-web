<?php
require_once("bootstrap.php");
if(loginCheck() == false){
    header("Location: login.php");
    exit;
}
$dbResult = $db->searchUser($_SESSION["user_id"]); // remember to op the user
if(!$dbResult || !$dbResult[0]["admin"]){
    header("Location: login.php");
    exit;
}
//Base Template
$templateParams["mainclass"] = "admin";
$templateParams["content"] = "admintemplate.php";
$templateParams["title"] = "Admin";
$templateParams["js"] = array(ADMIN_DIR."adminUtils.js");
$templateParams["search-options"] = array("Users","Events");

require TEMPLATE.'/base.php';
?>
