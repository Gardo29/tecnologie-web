<?php 
$notification["time_of_notification"] = DateTime::createFromFormat("Y-m-d H:i:s",$notification["time_of_notification"])->format("d/m H:i");
?>
<ul>
    <li class="text-container">
        <ul>
            <li class=title><strong><?php echo $notification["event_title"];?></strong></li><!--
         --><li class="date"><?php echo $notification["time_of_notification"] ?></li>
            <li class="message"><i>"<?php echo $notification["description"] ?>"</i></li>
        </ul>
    </li><!--
 --><li class="close-button-container">
        <button><strong class="fa fa-times close-icon"></strong></button>
    </li>
    <li class="user-id"><?php echo $notification["user_id"];?></li>
    <li class="notification-id"><?php echo $notification["notification_id"];?></li>
</ul>


