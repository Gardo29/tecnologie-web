<header>
    <h2>Cart Result</h2>
    <?php if(count($templateParams["failed"])==0) :?>
        <p>All purchased<p>
    <?php else: ?>
        <h3>Unpurchased</h3>
        <?php foreach($templateParams["failed"] as $fail):?>
            <p><?php echo $fail?></p>
        <?php endforeach;?>
    <?php endif ?>
    <p>Click <a href="index.php">here to Home</a> or <a href="cart.php">here to Cart</a></p>
</header>