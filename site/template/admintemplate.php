<h2>Admin</h2>
<ul>
    <li class="selection">
        <ul>
            <li class="select-container">
                   <select>
                       <?php foreach($templateParams["search-options"] as $option): ?>
                           <option value="<?php echo $option;?>"><?php echo $option;?></option>
                       <?php endforeach;?>
                   </select>
            </li><!--
         --><li class="searchbar-container"><label class="hidden" for="search">Filter</label><input type="text" id="search" name="search" placeholder="Filter"></li><!--
         --><li class="button-container"><button id="go">Search</button></li>
        </ul>
    </li>
</ul>
<ul class="results">
</ul>
