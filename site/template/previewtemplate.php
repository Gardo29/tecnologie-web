<?php 
    if(isset($templateParams["liked"]) && $templateParams["liked"] == true){
        $likeButton = "liked";
    }else{
        $likeButton = "";
    }
    $event["start_date_time"] = dateConverter($event["start_date_time"],"D j M H:i");
    $event["end_date_time"] = dateConverter($event["end_date_time"],"D j M H:i");
?>
<header>
    <div id="image-container">
        <img src="<?php echo $event["image"]?>" alt=""/>
    </div><!--
 --><section class="main-informations">
        <?php if($templateParams["active"]):?>
            <button id="preferred" class="<?php echo $likeButton;?>">
                <strong class="fa fa-heart"></strong>
            </button>
        <?php endif;?>
        <h2><?php echo $event["title"];?></h2>
        <h3>by author </br><?php echo $event["first_name"]." ".$event["last_name"]?> </h3>
        <ul class="state">
            <?php if($templateParams["active"]) :?>
                <li>State: Available</li><!--
            --><li>Price: <?php echo $event["price"]?>€</li>
            <?php else :?>
                <li>State: Expired</li>
            <?php endif?>

        </ul>
    </section>
    <hr>
    <section class="informations-box">
        <ul class="informations">
            <li><div class="data-box">
                <ul>
                    <li>Date and time:</li><!--
                 --><li>Starts: <?php echo $event["start_date_time"];?></li><!--
                 --><li>Ends:&nbsp&nbsp <?php echo $event["end_date_time"];?></li>
                </ul>
                </div>
            </li><!--
          --><li><div class="data-box">
                <ul>
                    <li>Location:</li><!--
                 --><li><?php echo $event["address"];;?></li><!--
                 --><li><?php echo $event["city"];;?></li>
                </ul>
                </div>
            </li>
        </ul>
        <?php if($templateParams["active"]):?> 
            <button id="tickets"><?php echo $templateParams["button"];?></button>
        <?php endif;?>
    </section>
</header>
<hr>
<section class="description">
    <h3>Description</h3>
    <p>
        <?php echo $event["description"];?>
    </p>
</section>
<section class="hidden">
    <p class="user-id"><?php echo $_SESSION["user_id"];?></p>
    <p class="event-id"><?php echo $_GET["event_id"];?></p>
</section>
