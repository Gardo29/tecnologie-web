<header>
    <div class="login-logo-container">
        <img src="<?php echo UPLOAD_DIR."menu-images/E20_logo2.png";?>" alt=""/>
    </div>
    <h2>Let's start!</h2>
    <p>To get started, Log in via Facebook or with an e-mail address.</p>
</header>
<?php if(isset($_GET["error"])){
    require(TEMPLATE."formError.php");
}
?>
<form action="<?php echo LOGIN_DIR."process_login.php";?>" method="post" name="login_form">
    <label for="login-email">Email</label>
    <label for="password">Password</label>
    <input type="email" id="login-email" placeholder="E-mail" name="login-email"/>
    <input type="password" id="password" placeholder="Password" name="password"/>
    <div class="button-container"> 
        <button id="continue">Login</button>
    </div>
</form> 
<footer>
    <p>New to E20? <a href="register.php">Sign up now!</a></p>
</footer>
