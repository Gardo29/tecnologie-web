<?php 
    $action = $templateParams["action"];
    if($action == "create"){
        $start = date("Y-m-d");
        $startTime = date("H:i");
        $end = date("Y-m-d");
        $endTime = date("H:i");
    }else{
        $start = dateConverter($event["start_date_time"],"Y-m-d");
        $startTime = dateConverter($event["start_date_time"],"H:i");
        $end = dateConverter($event["end_date_time"],"Y-m-d");
        $endTime = dateConverter($event["end_date_time"],"H:i");

    }
?>
<form action="<?php echo EVENT_DIR;?>processEvent.php" method="POST" enctype="multipart/form-data">
    <h2>Manage Event</h2>
    <?php if(isset($_GET["msg"])):?>
        <h3><?php echo $_GET["msg"]?></h3>
    <?php endif;?>
    <ul>
        <?php if($action == "modify"): ?>
            <li class="image">
                <img src="<?php echo USER_RES.$event["user_id"]."/".$event["image"]; ?>" alt="" />
            </li>
        <?php endif; ?>
        <li>
            <label for="title">Title:</label>
            <input type="text" id="title" name="title" placeholder="Title" value="<?php echo $event["title"]; ?>"/>
        </li>
        <li>
            <ul class="location">
                <li>
                    <label for="address">Location:</label>
                    <input type="text" id="address" name="address" placeholder="Address" value="<?php echo $event["address"]; ?>"/>
                </li><!--
             --><li>
                    <label class="hidden" for="city">City:</label>
                    <input type="text" id="city" name="city" placeholder="City" value="<?php echo $event["city"]; ?>"/>
                </li>
            </ul>
        </li>
        <li class="date-box">
            <ul>
                <li class="left">
                    <label for="start">Starts</label>
                    <input type="date" name="start" id="start" value="<?php echo $start ?>"/>
                </li>
                <li class="right">
                    <label for="start-time">Time</label>
                    <input type="time" name="start-time" id="start-time" value="<?php echo $startTime ?>"/>
                </li>
            </ul>
        </li><!--
     --><li class="date-box">
            <ul>
                <li class="left">
                    <label for="end">Ends</label>
                    <input type="date" name="end" id="end" value="<?php echo $end ?>"/>
                </li>
                <li class="right">
                    <label for="end-time">Time</label>
                    <input type="time" name="end-time" id="end-time" value="<?php echo $endTime ?>"/>
                </li>
            </ul>
        </li>

        <li>
            <label for="description">Event Description:</label>
            <textarea id="description" name="description" placeholder="Description"><?php echo $event["description"]; ?></textarea>
        </li>
        <li>
            <label for="event_image" class="button">Load image</label>
            <input type="file" name="event_image" id="event_image"/>
        </li>
        <li>
            <ul class="tickets">
                <li class="left">
                    <label for="tickets">Tickets</label>
                    <input type="number" name="tickets" id="tickets" placeholder="Number" value="<?php echo $event["ticket_number"];?>"/>
                </li>
                <li class="right">
                    <label for="price">Price</label>
                    <input type="number" name="price" id="price" placeholder="€" value="<?php echo $event["price"];?>"/>
                </li>
            </ul> 
        </li>
        <?php if($action == "modify"): ?>
            <li>
                <label for="notification">Message For Notification:</label>
                <textarea id="notification" name="notification" placeholder="Message"></textarea>
            </li>
        <?php endif; ?>
        <li>
            <label for="category">Category</label>
            <select id="category" name="category">
                <?php foreach($categories as $category): ?>
                    <option value="<?php echo $category;?>" <?php if($category == $event["category"]) echo "selected";?>><?php echo $category;?></option>
                <?php endforeach;?>
            </select>
        </li>
        <li>
            <button class="option-button"><?php echo ucfirst($action); ?></button><!--
         --><a class="option-button" href="eventsmanagement.php">Cancel</a> <!-- TODO: ref to events management--->
        </li>
    </ul>
        <?php if($_GET["action"]=="modify"): ?>
        <input type="hidden" name="event_id" value="<?php echo $event["event_id"]; ?>" />
        <input type="hidden" name="oldimg" value="<?php echo $event["image"]; ?>" />
        <?php endif;?>
        <input type="hidden" name="action" value="<?php echo $action; ?>" />

</form>
