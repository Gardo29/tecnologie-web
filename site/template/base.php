<?php
require_once("bootstrap.php");

if(loginCheck()){ 
    $user_state_link = LOGIN_DIR."logout.php";
    $value = "Logout";
}else{
    $user_state_link = "login.php";
    $value = "Login";
}
$menuvariables["mainmenu"] = array(array("link"=>"index.php","value"=>"Home"),array("link"=>"myevent.php?action=create","value"=>"Create"),array("link"=>"eventsmanagement.php","value"=>"My Events"),array("link"=>"eventsmanagement.php?select=2","value"=>"Saved"));
$menuvariables["usermenu"] = array(array("link"=>"notifications.php","value"=>"Notifications"),array("link"=>"profile.php","value"=>"Account"),array("link"=>$user_state_link,"value"=>$value));
array_unshift($templateParams["js"],"https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js","https://cdnjs.cloudflare.com/ajax/libs/jquery-touch-events/1.0.5/jquery.mobile-events.js",JS_DIR."menu.js");


if(loginCheck()){
    $dbResult = $db->searchUser($_SESSION["user_id"]);
    if($dbResult[0]["admin"]){
        array_unshift($menuvariables["usermenu"],array("link"=>"admin.php","value"=>"Admin"));
    }
}
require("base_content.php");
?>
