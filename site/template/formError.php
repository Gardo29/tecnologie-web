<section class="error-box">
    <?php switch($_GET["error"]){
        case DB_ERROR: ?>
            <p>Database Error</p>
        <?php break;
        case USERNAME_ALREADY_TAKEN:?>
            <p>Username Already Taken</p>
        <?php break;
        case EMPTY_FIELD:?>
            <p>Fill All Fields</p>
        <?php break;
        case WRONG_PASSWORD:?>
            <p>Wrong Password</p>
        <?php break;
        case INEXISTING_USER:?>
            <p>Inexisting User</p>
        <?php break; 
        case DISABLED_USER:?>
            <p>Disabled User</p>
        <?php break;
        default:
        break;
    }?>
</section>
