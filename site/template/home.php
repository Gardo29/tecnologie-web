<?php if(isset($categories)):
        $rand_keys = array_rand($categories, 4);
        foreach($rand_keys as $cat_index): $category = $categories[$cat_index];
        $events = $db->getAvailableEventByCategory($category, 0, 1);
        if(!empty($events)) :
    ?>
        <section>
            <h2><?php echo $category;?></h2>
            <div class="wrapper">
            <?php
                for ($i = 1; $i <= 3; $i++):
            ?>
                <section class="section<?php echo $i;?>">
                    
                <?php
                    $offset = ($i-1)*3;
                    $events = $db->getAvailableEventByCategory($category, $offset, 3);
                    foreach($events as $event):
                ?>
                    <div class="event">
                        <a href="preview.php?event_id=<?php echo $event["event_id"]; ?>">
                            <div class="cover">
                                <img src="<?php echo USER_RES.$event["user_id"]."/".$event["image"]; ?>" alt="" />
                            </div>
                            <div class="details">
                                <h3><?php echo $event["title"]; ?></h3>
                                <p>Author: <?php echo $event["first_name"]; ?> <?php echo $event["last_name"]; ?></p>
                                <p>Date: <?php echo dateConverter($event["start_date_time"],"d-m-Y \a\\t H:i"); ?></p>
                                <p>Place: <?php echo $event["city"]; ?></p>
                                <p>Cost: <?php echo $event["price"];?>€</p>
                                <p><?php echo $event["description"]; ?></p>
                            </div>
                        </a>
                    </div>
                <?php 
                    endforeach;
                ?>
                </section>
            <?php 
                endfor;
            ?>
            </div>
        </section>
    <?php
        endif; 
        endforeach;
    endif;
?>