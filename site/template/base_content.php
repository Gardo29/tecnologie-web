<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"/>
    <link rel="stylesheet" type="text/css" href="style/base.css" />
    <link rel="stylesheet" type="text/css" href="style/home.css" />
    <link rel="stylesheet" type="text/css" href="style/login.css" />
    <link rel="stylesheet" type="text/css" href="style/eventmanager.css" />
    <link rel="stylesheet" type="text/css" href="style/notifications.css" />
    <link rel="stylesheet" type="text/css" href="style/myevent.css" />
    <link rel="stylesheet" type="text/css" href="style/preview.css" />
    <link rel="stylesheet" type="text/css" href="style/admin.css" />
    <link rel="stylesheet" type="text/css" href="style/cart.css" />
    <link rel="stylesheet" type="text/css" href="style/search.css" />


    <?php if(isset($templateParams["js"])):
        foreach($templateParams["js"] as $script):
    ?>
        <script src="<?php echo $script; ?>"></script>
    <?php 
        endforeach;
    endif;
    ?>
    <title><?php echo $templateParams["title"];?></title>
</head>
<body>
    <header>
        <nav>
            <section class="mobile">
                <h2>Mobile menu</h2>
                <ul class="nav-bar">
                    <li><label class="hidden" for="mobile-search-bar">Search</label></li>
                    <li class="search-bar-container"><input type="text" placeholder="Search" id="mobile-search-bar" name="mobile-search-bar"/></li>
                    <li class="menu-button-container">
                        <button id="mobile-menu">
                            <strong class="fa fa-bars fa-2x menu-icon" aria-hidden="true"></strong>
                            <?php if(isset($notifications) && count($notifications)>0):?>
                                <span class="notifications-number"><?php echo count($notifications); ?></span>
                            <?php endif;?>
                        </button>
                    </li>
                    <li class="cart-button-container">
                        <button onclick="location.href='cart.php'"><strong class="fa fa-shopping-cart fa-2x"></strong></button>  
                        <span class="cart-number"><?php echo count($cart)?></span>
                    </li>
                    <li class="search-button-container"><button id="mobile-search-button"><strong class="fa fa-search fa-2x menu-icon"></strong></button></li>
                    <li class="image-container"><a href="index.php"><img src="<?php echo MENU_RES."E20_logo2.png"?>" alt=""/></a></li>
                </ul>
                <ul class="menu">
                    <li class="close-button-container"><button id="mobile-close-menu"><strong class="fa fa-times fa-2x menu-icon"></strong></button></li>
                    <?php if(isset($menuvariables["mainmenu"])):
                            foreach($menuvariables["mainmenu"] as $mainmenuitem):?>
                                <li><a href="<?php echo $mainmenuitem["link"];?>"><?php echo $mainmenuitem["value"];?></a></li>
                    <?php endforeach; endif;?>
                    <li><hr></li>
                    <?php if(isset($menuvariables["usermenu"])):
                            foreach($menuvariables["usermenu"] as $usermenu):?>
                                <li><a href="<?php echo $usermenu["link"];?>"><?php echo $usermenu["value"];?></a></li>
                    <?php endforeach; endif;?>
                </ul>
            </section>
            <section class="desktop">
                <h2>Desktop menu</h2>
                <ul class="nav-bar">
                    <li class="image-container"><a href="index.php"><img src="<?php echo MENU_RES."E20_logo2.png"; ?>" alt=""/></a></li>
                    <li class="main-menu"><ul>
                        <?php if(isset($menuvariables["mainmenu"])): ?>
                        <!--
                            <?php foreach($menuvariables["mainmenu"] as $mainmenuitem):?>
                                --><li><a href="<?php echo $mainmenuitem["link"];?>"><?php echo $mainmenuitem["value"];?></a></li><!--
                            <?php endforeach;?>
                        -->
                        <?php endif;?>
                    </ul></li>
                    
                    
                    <li class="account-button-container">
                        <button id="desktop-account-menu">
                            <strong class="fa fa-user-circle-o fa-2x menu-icon"></strong>
                            <?php if(isset($notifications) && count($notifications)>0):?>
                                <span class="notifications-number"><?php echo count($notifications); ?></span>
                            <?php endif;?>
                        </button>
                        <ul class="account-menu">
                            <?php if(isset($menuvariables["usermenu"])):
                                foreach($menuvariables["usermenu"] as $usermenu):?>
                                    <li><a href="<?php echo $usermenu["link"];?>"><?php echo $usermenu["value"];?></a></li>
                            <?php endforeach; endif;?>
                        </ul>
                    </li>
                    <li class="cart-button-container">
                        <button onclick="location.href='cart.php'"><strong class="fa fa-shopping-cart fa-2x"></strong></button>  
                        <span class="cart-number"><?php echo count($cart)?></span>  
                    </li>
                    <li class="search-button-container"><button id="desktop-search-button"><strong class="fa fa-search fa-2x menu-icon"></strong></button></li>
                    <li><label class="hidden" for="desktop-search-bar">Search</label></li>
                    <li class="search-bar-container"><input type="text" placeholder="Search" id="desktop-search-bar" name="desktop-search-bar"/></li>
                </ul>
            </section>
        </nav>
    </header>
    <main class="<?php echo $templateParams["mainclass"]?>">
        <?php if(isset($templateParams["content"])){
            require $templateParams["content"]; 
        }?>    
    </main>
    <footer>

        <ul>
            <li><a href="https://www.unibo.it/sitoweb/danilo.pianini">Pianini</a></li><!--
         --><li><a href="#">Contacts</a></li><!--
         --><li><a href="http://www.battletendency.altervista.org">JOJO</a></li><!--
         --><li><a href="https://www.google.it/search?q=raccoon&sxsrf=ACYBGNQEGYHlUtnMurm2qLEW1gUMt_EwYA:1579816835606&source=lnms&tbm=isch&sa=X&ved=2ahUKEwj1nq-q3JrnAhWOKVAKHYOZDnsQ_AUoAXoECBIQAw&biw=1280&bih=578">Raccoon</a></li>
        </ul>
        <ul>
            <li><a href=""><strong class="fa fa-facebook-square"></strong></a></li><!--
         --><li><a href=""><strong class="fa fa-instagram"></strong></a></li>
        </ul>
        <p>© 2019 Copyright E20</p>
    </footer>
</body>
</html>
