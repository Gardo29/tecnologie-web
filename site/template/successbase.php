<header>
    <h2><?php echo $templateParams["phrase"];?></h2>
    <p>Click <a href="index.php">here to Home</a> 
        <?php foreach($templateParams["links"] as $link) :?>
        or <a href="<?php echo $link["link"]?>"><?php echo $link["text"]?></a>
        <?php endforeach;?>
    </p>
</header>
