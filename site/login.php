<?php
require_once("bootstrap.php");

//Base Template
$templateParams["mainclass"] = "login";
$templateParams["content"] = TEMPLATE."login.php";
$templateParams["title"] = "Login";
$templateParams["js"] = array(LOGIN_DIR."formUtility.js",LOGIN_DIR."submitForm.js",LOGIN_DIR."sha512.js");

require TEMPLATE.'/base.php';
?>
