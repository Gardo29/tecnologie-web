<?php
require_once("bootstrap.php");

if(!isset($_GET["value"])){
    header("Location: index.php");
    exit;
}
//Base Template
$templateParams["mainclass"] = "search";
$templateParams["content"] = "searchtemplate.php";
$templateParams["title"] = "Search";
$templateParams["js"] = array(JS_DIR."drawEvent.js",JS_DIR."search.js",CART_DIR."processedbuttons.js",SAVED_DIR."processcart.js");


require TEMPLATE.'/base.php';
?>
