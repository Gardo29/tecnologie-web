<?php
require_once("bootstrap.php");
if(!loginCheck()){
    header("Location: login.php");
    exit;
}
//Base Template
$templateParams["mainclass"] = "cart";
$templateParams["content"] = TEMPLATE."carttemplate.php";
$templateParams["title"] = "Cart";
$templateParams["js"] = array(JS_DIR."drawEvent.js",CART_DIR."processedbuttons.js",CART_DIR."price.js");

$total = 0;
$cartevents = array();
foreach($cart as $singleEvent){
    $event = $db->getEventById($singleEvent["event_id"])[0];
    array_push($cartevents,$event);
    $total += $event["price"];
}
require TEMPLATE.'/base.php';
?>
