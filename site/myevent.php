<?php
require_once("bootstrap.php");

if(loginCheck() == false){
    header("Location: login.php");
    exit;
}
if(  !isset($_GET["action"]) || 
    (!isset($_GET["event_id"]) && $_GET["action"] == "modify") ||
    ($_GET["action"]!="create" && $_GET["action"]!="modify" && $_GET["action"]!="delete")){
    header("Location: index.php");
    exit;
}
if(isset($_GET["event_id"])){
    $event = $db->getEventById($_GET["event_id"]);
    if(!$event || count($event) == 0){
        header("Location: index.php");
    }
    $event = $event[0];

}
if(isset($_GET["action"]) === "modify" && !isset($_GET["event_id"])){
    header("Location: index.php");
}
//Base Template
$templateParams["mainclass"] = "myevent";
$templateParams["content"] = TEMPLATE."eventform.php";
$templateParams["title"] = "My Event";
$templateParams["js"] = array();
$templateParams["action"] = $_GET["action"];

if($templateParams["action"] === "create"){
    $event = emptyEvent();
}

require TEMPLATE.'/base.php';

function emptyEvent(){
    return array("price"=>"","ticket_number" => "","title" => "","location" => "","address" => "","city" => "","state" => "", "image" => "", "description" => "", "category" => "");
}
?>
