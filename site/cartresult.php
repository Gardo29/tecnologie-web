<?php
require_once("bootstrap.php");
//Base Template
$templateParams["mainclass"] = "success";
$templateParams["title"] = "Cart Result";
$templateParams["content"] = TEMPLATE."cartresulttemplate.php";
$templateParams["js"] = array();
if(isset($_GET["unpurchased"])){
    $templateParams["failed"] = explode(",",$_GET["unpurchased"]);
}else{
    $templateParams["failed"] = array();
}

require TEMPLATE.'/base.php';
?>
