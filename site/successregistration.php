<?php
require_once("bootstrap.php");
//Base Template
if(!isset($_GET["success"],$_GET["operation"]) || $_GET["success"] != true || ($_GET["operation"] !="profile" && $_GET["operation"] !="registration")){
    header("Location: index.php");
}
$templateParams["mainclass"] = "success";
$templateParams["title"] = "Registration successful";
$templateParams["content"] = TEMPLATE."successbase.php";
$templateParams["js"] = array();

$templateParams["links"] = array();

if($_GET["operation"] == "registration"){
    $templateParams["phrase"] = "Successful Registration </br> Thank you!";
    array_push($templateParams["links"],array("link"=>"login.php","text"=>"here to Login"));
}
if($_GET["operation"] == "profile"){
    $templateParams["phrase"] = "Change Successful!";
}

require TEMPLATE.'/base.php';
?>
