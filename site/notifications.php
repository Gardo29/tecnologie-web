<?php
require_once("bootstrap.php");
if(loginCheck() == false){
    header("Location: login.php");
    exit;
}
//Base Template
$templateParams["mainclass"] = "notifications";
$templateParams["content"] = "notificationtemplate.php";
$templateParams["title"] = "Notifications";
$templateParams["js"] = array(NOTIFICATIONS_DIR."notificationsUtils.js");
require TEMPLATE.'/base.php';
?>
